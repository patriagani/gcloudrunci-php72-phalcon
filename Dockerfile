FROM patriagani/php7.2-mongodb-phalcon3

RUN sed -i 's/80/${PORT}/g' /etc/apache2/sites-available/site.conf /etc/apache2/ports.conf
COPY index.php /var/www/html
